<?php
//require ('./vendor/autoload.php');

require_once 'include/dbHandler.php';
require_once 'include/gdprClass.php';

require_once 'lib/Slim/Slim/Slim.php';


Slim\Slim::registerAutoloader();

$app = new Slim\Slim();
/*
$corsOptions = array(
    "origin" => array('*'),
    //"exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    //"maxAge" => 1728000,
    //"allowCredentials" => True,
    "allowMethods" => array("POST, GET, OPTIONS"),
    "allowHeaders" => array("x-requested-with, Content-Type, origin, authorization, accept, client-security-token")
	);
*/
	require ('CorsSlim.php');
//$app->add(new \CorsSlim\CorsSlim($corsOptions));

$app->get('/userlists', function() {
	$db = new dbHandler();
	$cur = $db->getUsersList();
	$result = array();
	foreach ($cur as $doc) {
	$tmp = array();
	array_push($result,$doc);
	}
	response(200, $result);
});

//Post Friends end point
$app->post('/addGdprUser', function() use($app){
	$res = array();	
	$db = new dbHandler();
	$gdpr = new gdpr();
	$res = $db->insertUser($app);
	
	if($res['error']==false){
	    $inputData['emailid'] = $res['email'];
      unset($res['email']);
	    $inputData['subject'] = "";
	    $response = $gdpr->mailerApiForEmailVerification($inputData);      
      if($response['status']=='SUCCESS'){
          $res['error'] = false;
          $res['message'] = $response['message'];
      }else{
          $res['error'] = true;
          $res['message'] = $response['message'];
      }
	  }
	response(200, $res);
});




$app->post('/changeVerificationStatus', function() use($app){
	//print_r($_SERVER);
	$db = new dbHandler();
	$res = array();	
	$cType = $app->request->headers('Content-Type');
	if (strpos('application/json', $cType) !== false || strpos('application/json; charset=UTF-8', $cType) !== false) {
		  $body = $app->request->getBody();
		  $requestData = json_decode($body, true);
	}else if (strpos('application/x-www-form-urlencoded', $cType) !== false) {
		  $requestData = $app->request->post();  
	}else{
		  $bodyData = $app->request->getBody();            
		  parse_str($bodyData, $requestData);
	}
	
  if($requestData['email']==''){
    $requestData = $app->request->post();  
  }
  
	// CALL SSO API HERE AND PERFORM ACCORDINGLY
  $res = $db->isSSOVerificationSuccess($requestData);
	if($res['error']==false){
		$dataToBeUpdated = array('verification_status' => 1);
		$db = new dbHandler();
		$email = $requestData['email']; //
		$res = $db->updateUserDetailsStatus($email, $dataToBeUpdated);
		if($res['error'] == false){
			$res["message"] = "EMAIL_VERIFIED_SUCCESSFULLY";	
		}
	}
	response(200, $res);
});



//rest response helper
function response($status, $response) {
	$app = \Slim\Slim::getInstance();
	//Set http response code
	$app->status($status);
	//Set content type
	$app->contentType('application/json');
	//Encode result as json
	echo json_encode($response, JSON_PRETTY_PRINT);

}

//run application
$app->run();
?>

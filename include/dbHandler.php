<?php
class dbHandler {
      private $con;
      private $col;
      
      function __construct() {
            require_once dirname(__FILE__) . '/dbConnect.php';
            $db = new dbConnect();            
            $this->con = $db->connect();
      }

      
      public function getUsersList() {
            $filter = [];
            $options = ['sort' => ['_id' => -1],];
            $query = new MongoDB\Driver\Query($filter, $options);
            $cur = $this->con->executeQuery(userRequestDetails, $query);
            return $cur;
      }

      // this function is being used in multi places
      public function gerUserByEmail($email) {
            $filter = array('email' => $email);
            $options = ['sort' => ['_id' => -1],];
            $query = new MongoDB\Driver\Query($filter, $options);
            $cur = $this->con->executeQuery(userRequestDetails, $query);
            return $cur;
      }

      function isValidEmail($email){
            $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';             
            if (preg_match($pattern, $email) === 1) { // emailaddress is valid 
                  return true;
            }
            return false;
      }

      function isAlreadyRequested($email){
            // check already requested
            $userDetails = $this->gerUserByEmail($email);
            $userDetails = current($userDetails->toArray());
            
            if(!empty($userDetails)){      
                  return true;
            }
            return false;
      }

      

      function updateUserDetailsStatus($email, $dataToBeUpdated){    
            try {
                  $userDetails = $this->gerUserByEmail($email);
                  $userDetails = current($userDetails->toArray());
                  if(empty($userDetails)){
                        $res["error"] = TRUE;
                        $res["message"] = "USER_NOT_EXISTS";
                        return $res;
                  }

                  if($userDetails->verification_status == 1) {
                        $res["error"] = TRUE;
                        $res["message"] = "USER_ALREADY_VERIFIED";
                        return $res;
                  }
                  
                  $this->updateUserOptions($email, $dataToBeUpdated);
                  $res["error"] = FALSE;
                  $res["message"] = "USER_UPDATED";
                  return $res;
            }
            catch (MongoCursorException $e) {
                  $res["error"] = TRUE;
                  $res["message"] = "SOMETHING_WENT_WRONG";
                  return $res;
            }
      }

      function updateUserOptions($email, $dataToBeUpdated){
            $filter = ['email' => $email];
            $updateOptions = ['multi' => true, 'upsert' => false];
            $bulk = new MongoDB\Driver\BulkWrite;
            $newObj = ['$set' => $dataToBeUpdated];
            $bulk->update ( $filter , $newObj, $updateOptions );
            $result = $this->con->executeBulkWrite(userRequestDetails, $bulk);
            return $result;
      }

      function updateUserDetailsCode($email, $dataToBeUpdated){
            try {
                  $userDetails = $this->gerUserByEmail($email);
                  $userDetails = current($userDetails->toArray());
                  if(empty($userDetails)){
                        $res["error"] = TRUE;
                        $res["message"] = "USER_NOT_EXISTS";
                        return $res;
                  }

                  $this->updateUserOptions($email, $dataToBeUpdated);
                  $res["error"] = FALSE;
                  $res["message"] = "USER_UPDATED";
                  return $res;
            }
            catch (MongoCursorException $e) {
                  $res["error"] = TRUE;
                  $res["message"] = "SOMETHING_WENT_WRONG";
                  return $res;
            }
      }

      public function insertUser($app) {
            //echo $app->request->getMethod();
           $cType = $app->request->headers('Content-Type');
            if (strpos('application/json', $cType) !== false  || strpos('application/json; charset=UTF-8', $cType) !== false) {
                  $body = $app->request->getBody();
                  $requestData = json_decode($body, true);
            }else if (strpos('application/x-www-form-urlencoded', $cType) !== false) {
                  $requestData = $app->request->post();  
            }else{
                  $bodyData = $app->request->getBody();            
                  parse_str($bodyData, $requestData);
            }
            
            if(!isset($requestData['email']) || empty($requestData['email'])){
                  $res["error"] = true;
                  $res["message"] = "EMAIL_ID_REQUIRED";
                  return $res;
            }
            $email = $requestData['email']; // *
            if(!$this->isValidEmail($email)){
                  $res["error"] = true;
                  $res["message"] = "INVALID_EMAIL";
                  return $res;
            }     
            
            $domain = "";       
            $columbia_id = "";
            if(isset($requestData['domain']) && !empty($requestData['domain'])){
                  $domain = $requestData['domain']; // *
            }
            if(isset($requestData['columbia_id']) && !empty($requestData['columbia_id'])){
                  $columbia_id = $requestData['columbia_id'];
            }
            
            if(!isset($requestData['action']) || empty($requestData['action'])){
                  $res["error"] = true;
                  $res["message"] = "ACTION_REQUIRED";
                  return $res;
            }

            $action = $requestData['action']; 
            $possibleActions = array('DELETE', 'DOWNLOAD','BOTH');
            if(!in_array($action, $possibleActions)){
                  $res["error"] = true;
                  $res["message"] = "INVALID_ACTION";
                  return $res;
            }
            $SDK_ids = "";
            //print_r($requestData['SDK_ids']);
            if(isset($requestData['SDK_ids']) && !empty($requestData['SDK_ids'])){
                  $SDK_ids = $requestData['SDK_ids'];
            }
            $app_ids = "";
            if(isset($requestData['app_ids']) && !empty($requestData['app_ids'])){
                  $app_ids = $requestData['app_ids'];
            }

            $document = array(
                  "email" => $email,
                  "verification_status" => 0,
                  "verification_code" => "",
                  "columbia_id" => $columbia_id,
                  "action" => $action,
                  "domain" => $domain,
                  "SDK_ids" => $SDK_ids,
                  "app_ids" => $app_ids,
                  "created" => date('Y-m-d H:i:s'),
                  "modified" => date('Y-m-d H:i:s')
            );
            $res["email"] = $email;
            if($this->isAlreadyRequested($email)){      
                  $this->updateUserOptions($email, $document);
                  $res["error"] = FALSE;
                  $res["message"] = "USER_ADDED_SUCCESS";
                  return $res;
            }
            //Insert to collection
            try {
                  $bulk = new MongoDB\Driver\BulkWrite;
                  $bulk->insert($document);
                  $this->con->executeBulkWrite(userRequestDetails, $bulk);                  
                  $res["error"] = FALSE;
                  $res["message"] = "USER_ADDED_SUCCESS";
                  return $res;
            }
            catch (MongoCursorException $e) {
                  $res["error"] = TRUE;
                  $res["message"] = "USER_ADDED_FAILED";
                  return $res;
            }
      }

      function isSSOVerificationSuccess($requestData){
           // print_r($requestData);
            if(!isset($requestData['email']) || empty($requestData['email'])){
                  return false;
            }
            $email = $requestData['email']; // *
            if(!$this->isValidEmail($email)){
                  return false;
            }
            if(!isset($requestData['code']) || empty($requestData['code'])){
                  return false;
            }
           $code = $requestData['code']; // *
            // TODO Call SSO API for further verification
            
            require_once dirname(__FILE__).'/gdprClass.php';
            $gdpr = new gdpr();
            $response = $gdpr->otpVerification($email,$code);
            
            if($response['status']=='SUCCESS')
            {
                  $res['error'] = false;
                  $res['message'] = $response['message'];
              }else{
                  $res['error'] = true;
                  $res['message'] = $response['message'];
              }
            return $res;
      }
}
?>

<?php
class dbConnect {
	private $con;
	private $db;

	function __construct() {}

	function connect() {
			include_once dirname(__FILE__) . '/config.php';
			try {      
				$this->con = new MongoDB\Driver\Manager("mongodb://".DB_USER.":".DB_PASS."@".DB_HOST.":27017/".DB_NAME );
				$this->db = $this->con;
			}
			catch (MongoConnectionException $e) {
				echo "Cannot Connect to MongoDB";
			}
			return $this->db;
	}
}
?>

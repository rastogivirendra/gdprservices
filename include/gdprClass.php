<?php 

/**
* 
*/
class gdpr
{
    
    function mailerApiForEmailVerification($inputData)
    {

        $emailids = $inputData['emailid'];

        $curl = curl_init();
        $postField ='{  "email": "'.$emailids.'"}';

        curl_setopt($curl, CURLOPT_URL, "http://172.29.42.100:8888/services/otp/sendOtpEmail");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$postField);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                                                    'Content-type: application/json',
                                                    'apikey:cuBWPevx5cb7pmZJgLsCVLBKCZZ7v68Q'
                                                    )
                    );
        $curl_results = curl_exec ($curl);
        curl_close ($curl);
        $json = json_decode($curl_results, true);

        return $json;
    }

    function otpVerification($email,$code)
    {
        $curl = curl_init();
        $postField ='{  "emailId": "'.$email.'",
                        "otp":"'.$code.'"
                    }';

        curl_setopt($curl, CURLOPT_URL, "http://172.29.42.100:8888/services/otp/verifyOtpEmail");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS,$postField);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json',
                                                      'apikey:cuBWPevx5cb7pmZJgLsCVLBKCZZ7v68Q'  
                                                    )
                    );
        $curl_results = curl_exec ($curl);
        curl_close ($curl);
        $json = json_decode($curl_results, true);
        return $json;
    }
}

?>
